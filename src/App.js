import React, {Component} from 'react';
import ModalWindow from "./Component/ModalWindow";
import Buttons from "./Component/Buttons";
class App extends Component {
    state = {
        firstModal: false,
        secondModal: false,
    };

    showFirstModal = () => {
        this.setState({
            firstModal:!this.state.firstModal,
        });
    };

    showSecondModal = (e) => {
        this.setState({
            secondModal:!this.state.secondModal,
        });
    };

    render() {
        return (
            <div className="App">
                <Buttons text={"Open first modal"} color={"#b374a8"} clicked={this.showFirstModal}/>
                <Buttons text={"Open second modal"} color={"#b36d69"} clicked={this.showSecondModal}/>
                {
                    this.state.firstModal ?
                        <ModalWindow closed={this.showFirstModal}
                                     windowtext={"Once you delete this file, it won't be possible to undo this action.Are you sure you want to delete it?"}
                                     headertext={"Do you want to delete this file?"}
                                     action={[<Buttons text={"Ok"} color={"#b3382c"}/>,<Buttons text={"Cancel"} color={"#b3382c"}/>]}
                         /> : null
                }
                {
                    this.state.secondModal ?
                        <ModalWindow closed={this.showSecondModal}
                                     windowtext={"Once you exit this file, changes will not be saved."}
                                     headertext={"Do you want to Save this file?"}
                                     action={[<Buttons text={"Save"} color={"#4e8eb3"}/>,<Buttons text={"Don't Save"} color={"#4170b3"}/>]}
                        /> : null
                }
            </div>
        );
    }
}
export default App;
